//
//  WDDAppDelegate.h
//
//  Created by Wojtek Dmyszewicz on 01-05-13.
//  Copyright (c) 2013 com.WojtekDmyszewicz. All rights reserved.
//

#import <UIKit/UIKit.h>

@class WDDViewController;

@interface WDDAppDelegate : UIResponder <UIApplicationDelegate>

@property (strong, nonatomic) UIWindow *window;

@property (strong, nonatomic) WDDViewController *viewController;

@end
