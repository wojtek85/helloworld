//
//  WDDViewController.m
//
//  Created by Wojtek Dmyszewicz on 01-05-13.
//  Copyright (c) 2013 com.WojtekDmyszewicz. All rights reserved.
//


#import "WDDViewController.h"
#import "Person.h"

@interface WDDViewController ()

@end

@implementation WDDViewController

- (void)viewDidLoad
{
    [super viewDidLoad];
	// Do any additional setup after loading the view, typically from a nib.
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (IBAction)sayHelloButtonPressed:(id)sender
{
    Person *person = [[Person alloc] initWithFirstName:@"Jeff" lastName:@"Kelley" birthYear: 1986];
    
    [helloWorldLabelOne setText:[person displayName]];
    
   //[helloWorldLabel setText:@"Hello, World!"];
}

@end
