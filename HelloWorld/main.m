#import <UIKit/UIKit.h>

#import "WDDAppDelegate.h"

int main(int argc, char *argv[])
{
    @autoreleasepool {
        return UIApplicationMain(argc, argv, nil, NSStringFromClass([WDDAppDelegate class]));
    }
}