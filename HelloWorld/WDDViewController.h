//
//  WDDViewController.h
//
//  Created by Wojtek Dmyszewicz on 01-05-13.
//  Copyright (c) 2013 com.WojtekDmyszewicz. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface WDDViewController : UIViewController {
    IBOutlet UILabel *helloWorldLabel;
    IBOutlet UILabel *helloWorldLabelOne;
}

- (IBAction)sayHelloButtonPressed:(id)sender;

@end
