//
//  WDDPerson.m
//
//  Created by Wojtek Dmyszewicz on 01-05-13.
//  Copyright (c) 2013 com.WojtekDmyszewicz. All rights reserved.
//

#import "Person.h"

@implementation Person


- (id)initWithFirstName:(NSString *)fName
               lastName:(NSString *)lName
              birthYear:(NSInteger)bYear
{
    self = [super init];
    
    if(self){
        firstName = fName;
        lastName  = lName;
        birthYear = bYear;
    }
    
    return self;
}

- (NSString *)displayName
{
    NSString *name = [NSString stringWithFormat:@"%@, %@", lastName, firstName];
    NSString *year = [NSString stringWithFormat:@"%ld", (long)birthYear];
    
    NSArray  *array1 = [NSArray arrayWithObjects: name, year, nil];
    NSString *nameAndYear = [array1 componentsJoinedByString:@","];
    
    return nameAndYear;
}

- (NSString *)firstName
{
    return firstName;
}

- (NSString *)lastName
{
    return lastName;
}

- (NSInteger)birthYear
{
    return birthYear;
}

- (void)setFirstName:(NSString *)name
{
    firstName = name;
}

- (void)setLastName:(NSString *)name
{
    lastName = name;
}

- (void)setBirthYear:(NSInteger)year
{
    birthYear = year;
}

@end
