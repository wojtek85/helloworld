//
//  WDDPerson.h
//
//  Created by Wojtek Dmyszewicz on 01-05-13.
//  Copyright (c) 2013 com.WojtekDmyszewicz. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface Person : NSObject {
    NSString *firstName;
    NSString *lastName;
    NSInteger birthYear;
}

- (id)initWithFirstName:(NSString *)firstname
               lastName:(NSString *)lastname
              birthYear:(NSInteger)birthyear;

- (NSString *)firstName;
- (void)setFirstName:(NSString *)firstName;
- (NSString *)lastName;
- (void)setLastName:(NSString *)lastName;
- (NSInteger)birthYear;
- (void)setBirthYear:(NSInteger)birthYear;
- (NSString *)displayName;

@end
